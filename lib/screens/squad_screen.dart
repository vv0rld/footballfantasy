import 'package:flutter/material.dart';

import 'package:football_fantasy/constants/media.dart';

import 'package:football_fantasy/constants/spaces.dart';
import 'package:football_fantasy/models/player.dart';

import 'package:football_fantasy/widgets/header_widget.dart';
import 'package:football_fantasy/widgets/squad_widget.dart';
import 'package:football_fantasy/widgets/substitue_widet.dart';

class SquadScreen extends StatefulWidget {
  const SquadScreen({
    Key? key,
  }) : super(key: key);

  @override
  _SquadScreenState createState() => _SquadScreenState();
}

class _SquadScreenState extends State<SquadScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[400],
        body: SafeArea(
            child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
              HeaderWidget(
                rIcon: Icon(Icons.check_outlined),
                title: 'انتخاب تیم',
                lIcon: Icon(Icons.menu_outlined),
              ),
              Spaces.verticalSpaceLarge,
              Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Stack(clipBehavior: Clip.hardEdge, children: [
                    Container(
                      height: 450,
                      width: 450,
                      child: (Image.asset(
                        Media.Field,
                        fit: BoxFit.fitHeight,
                      )),
                    ),
                    SquadWidget(players: mockplayers),
                  ]),
                  Spaces.verticalSpaceTiny,
                  SubstituesWidget(players: mocksubs)
                ],
              ),
            ])));
  }
}

class PlayerCard extends StatelessWidget {
  final Player player;
  final onTap;

  const PlayerCard({
    Key? key,
    required this.player,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        // height: 94,
        width: 60,
        child: Column(children: [
          Container(width: 60, child: Image.asset(player.image)),
          Spaces.verticalSpaceTiny,
          Text(player.name,
              style: TextStyle(
                  fontSize: 12, fontWeight: FontWeight.bold, height: 1)),
          // Text(
          //   nextOppopnent,
          //   style:
          // //       TextStyle(fontSize: 12, fontWeight: FontWeight.bold, height: 1),
          // ),
        ]),
      ),
    );
  }
}
