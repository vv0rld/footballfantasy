import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:football_fantasy/models/player.dart';

class PlayerProfilePopup extends StatelessWidget {
  final Player player;
  const PlayerProfilePopup(BuildContext context,
      {Key? key, required this.player})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new AlertDialog(
        title: const Text('Popup example'),
        content: Container(
          height: 300,
          width: 300,
          child: Column(
            children: [
              Text(player.name),
              Text(player.clubName),
              Text(player.gamePoint.toString()),
              Text(player.position.toString()),
              Text(
                player.value.toString(),
              ),
              Container(child: Image.asset(player.image))
            ],
          ),
        ));
  }
}
