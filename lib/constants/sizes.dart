class Sizes {
  static const xxs = 4.0;
  static const xs = 8.0;
  static const s = 16.0;
  static const m = 32.0;
  static const l = 48.0;
  static const xl = 96.0;
}

const iconWidth = 40.0;
const notSelectedCircleIndicatorSize = 7.0;
const selectedCircleIndicatorSize = 8.0;
