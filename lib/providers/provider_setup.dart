import 'package:football_fantasy/providers/squad_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildWidget> providers = [
  ListenableProvider.value(value: SquadProvider())
];
