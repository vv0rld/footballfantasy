import 'package:flutter/widgets.dart';
import 'package:football_fantasy/models/player.dart';
import 'package:football_fantasy/models/squad.dart';

class SquadProvider extends ChangeNotifier {
  Squad? squad;

  SquadProvider() {
    // squad = Squad(subtitutes: MOCK_SUBS, players: MOCK_PLAYERS, captain: , teamValue: teamValue)
  }

  void fetchSquadFromLocalRepository() {}

  void saveSquadChangesInLocalRepository() {}

  void createSquad() {}

  void substitute(Player subIn, Player subOut) {
    //switches two players in squad's subsitutes and playing players
  }
}


// context.read<SquadProvider>.squad.players 