// To parse this JSON data, do
//
//     final squad = squadFromJson(jsonString);

import 'package:football_fantasy/models/player.dart';

import 'dart:convert';

Squad squadFromJson(String str) => Squad.fromJson(json.decode(str));

String squadToJson(Squad data) => json.encode(data.toJson());

class Squad {
  Squad({
    required this.substitutes,
    required this.players,
    // required this.captain,
    // required this.teamValue,
  });

  List<Player> substitutes;
  List<Player> players;
  // Player captain;
  // int teamValue;

  factory Squad.fromJson(Map<String, dynamic> json) => Squad(
        substitutes: List<Player>.from(
            json["subtitutes"].map((x) => Player.fromJson(x))),
        players:
            List<Player>.from(json["squad"].map((x) => Player.fromJson(x))),
        // captain: Player.fromJson(json["captain"]),
        // teamValue: json["teamValue"],
      );

  Map<String, dynamic> toJson() => {
        "subtitutes": List<dynamic>.from(substitutes.map((x) => x.toJson())),
        "players": List<dynamic>.from(players.map((x) => x.toJson())),
        // "captain": captain.toJson(),
        // "teamValue": teamValue,
      };
}

//MOCK SQUAD
Squad mocksquad = Squad(
  substitutes: mocksubs,
  players: mockplayers,
);
