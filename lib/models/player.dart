import 'package:football_fantasy/constants/media.dart';
import 'package:football_fantasy/enums/player_positions.dart';

// To parse this JSON data, do
//
//     final player = playerFromJson(jsonString);

import 'dart:convert';

Player playerFromJson(String str) => Player.fromJson(json.decode(str));

String playerToJson(Player data) => json.encode(data.toJson());

class Player {
  Player({
    required this.name,
    required this.clubName,
    required this.value,
    required this.gamePoint,
    required this.image,
    required this.position,
  });

  String name;
  String clubName;
  double value;
  double gamePoint;
  String image;
  PlayerPositions position;

  factory Player.fromJson(Map<String, dynamic> json) => Player(
        name: json["name"],
        clubName: json["clubName"],
        value: json["value"],
        gamePoint: json["gamePoint"].toDouble(),
        image: json["image"],
        position: playerPositionFromString(
          json["position"],
          staring11: json["staring11"],
          isSelected: json["isSelected"],
        ),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "clubName": clubName,
        "value": value,
        "gamePoint": gamePoint,
        "image": image,
        "position": position.toShortString(),
      };
}

//MOCK PLAYERS
List<Player> mockplayers = [
  new Player(
      name: 'حسینی',
      position: PlayerPositions.GK,
      clubName: 'Perspolis',
      value: 4,
      gamePoint: 1,
      image: Media.GKJersey),
  new Player(
      name: 'حسنی',
      position: PlayerPositions.DEF,
      clubName: 'Perspolis',
      value: 4,
      gamePoint: 1,
      image: Media.PJersey),
  new Player(
      name: 'اصغری',
      position: PlayerPositions.DEF,
      clubName: 'Perspolis',
      value: 4,
      gamePoint: 1,
      image: Media.PJersey),
  new Player(
      name: 'قربانی',
      position: PlayerPositions.DEF,
      clubName: 'Perspolis',
      value: 4,
      gamePoint: 1,
      image: (Media.PJersey)),
  new Player(
    name: 'شروه',
    position: PlayerPositions.DEF,
    clubName: 'Perspolis',
    value: 5,
    gamePoint: 2,
    image: (Media.PJersey),
  ),
  new Player(
      name: 'نوذری',
      position: PlayerPositions.MID,
      clubName: 'Perspolis',
      value: 4.6,
      gamePoint: 2,
      image: (Media.PJersey)),
  new Player(
      name: 'کاتوزیان',
      position: PlayerPositions.MID,
      clubName: 'Perspolis',
      value: 4.8,
      gamePoint: 2,
      image: (Media.PJersey)),
  new Player(
      name: 'رسولی',
      position: PlayerPositions.MID,
      clubName: 'Perspolis',
      value: 4.8,
      gamePoint: 2,
      image: (Media.PJersey)),
  new Player(
      name: 'فروزنده',
      position: PlayerPositions.MID,
      clubName: 'Esteghlal',
      value: 4.1,
      gamePoint: 3,
      image: (Media.PJersey)),
  new Player(
      name: 'بدر',
      position: PlayerPositions.ATT,
      clubName: 'Esteghlal',
      value: 4.9,
      gamePoint: 7,
      image: (Media.PJersey)),
  new Player(
      name: 'محمدعلی',
      position: PlayerPositions.ATT,
      clubName: 'Esteghlal',
      value: 1,
      gamePoint: 1,
      image: (Media.PJersey)),
  new Player(
    name: 'محمدی',
    position: PlayerPositions.GK,
    clubName: 'Perspolis',
    value: 4,
    gamePoint: 1,
    image: Media.GKJersey,
  ),
  new Player(
    name: 'محمدی',
    position: PlayerPositions.DEF,
    clubName: 'Sepahan',
    value: 2.5,
    gamePoint: 1,
    image: Media.PJersey,
  ),
  new Player(
    name: 'مجیدی',
    position: PlayerPositions.MID,
    clubName: 'Zob Ahan',
    value: 3.5,
    gamePoint: 3,
    image: Media.PJersey,
  ),
  new Player(
    name: 'علی زاده',
    position: PlayerPositions.MID,
    clubName: 'Foolad',
    value: 3.5,
    gamePoint: 3,
    image: Media.PJersey,
  ),
];

List<Player> mocksubs = [
  new Player(
      name: 'qww',
      clubName: 'qwe',
      value: 3.5,
      gamePoint: 2,
      image: Media.PJersey,
      position: PlayerPositions.ATT)
];
  //includes 4 substitute players

