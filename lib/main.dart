import 'package:flutter/material.dart';
import 'package:football_fantasy/models/player.dart';
import 'package:football_fantasy/providers/provider_setup.dart';
import 'package:football_fantasy/screens/squad_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(MultiProvider(
      providers: providers,
      child: MaterialApp(
        theme: ThemeData(fontFamily: 'YekanBakhFaNum'),
        debugShowCheckedModeBanner: false,
        title: "football fantasy",
        home: SquadScreen(),
      ),
    ));
