import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:football_fantasy/constants/sizes.dart';
import 'package:football_fantasy/models/player.dart';
import 'package:football_fantasy/screens/squad_screen.dart';

class SubstituesWidget extends StatelessWidget {
  const SubstituesWidget({
    Key? key,
    required this.players,
  }) : super(key: key);

  final List<Player> players;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: [
        Padding(
            padding: const EdgeInsets.only(right: Sizes.s),
            child: Row(
              textDirection: TextDirection.rtl,
              children: [InkWell(child: Text('تعویض'))],
            )),
        Builder(
          builder: (context) {
            return Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
//player cards builder
                ]);
          },
        )
      ],
    );
  }
}
