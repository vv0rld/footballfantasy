import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeaderWidget extends StatelessWidget {
  final String? title;
  final Icon? rIcon;
  final Icon? lIcon;

  const HeaderWidget({Key? key, this.rIcon, this.lIcon, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          textDirection: TextDirection.rtl,
          mainAxisSize: MainAxisSize.max,
          children: [
            InkWell(child: rIcon),
            Expanded(child: Container()),
            Text(
              title!,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Expanded(child: Container()),
            InkWell(
              child: lIcon,
            )
          ],
        ),
      ),
    );
  }
}
