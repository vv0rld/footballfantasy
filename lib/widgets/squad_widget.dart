import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:football_fantasy/constants/spaces.dart';
import 'package:football_fantasy/models/player.dart';
import 'package:football_fantasy/screens/squad_screen.dart';

Widget _buildPopupDialog(
  Player player,
  BuildContext context,
) {
  return new AlertDialog(
    title: const Text('Popup example'),
    content: new Container(
      height: 300,
      width: 300,
      child: Column(
        children: [
          Text('${player.name}'),
          Text(player.clubName),
          Text(player.gamePoint.toString()),
          Text(player.position.toString()),
          Text(
            player.value.toString(),
          ),
          Container(
            child: Image.asset(player.image),
          ),
        ],
      ),
    ),
  );
}

class SquadWidget extends StatelessWidget {
  const SquadWidget({
    Key? key,
    required this.players,
  }) : super(key: key);

  final List<Player> players;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: double.maxFinite,
        // height: 100,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                PlayerCard(
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (context) =>
                          _buildPopupDialog(players[0], context),
                    );
                  },
                  player: players[0],
                )
              ],
            ),
            Spaces.verticalSpaceTiny,
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  PlayerCard(
                    player: players[1],
                  ),
                  PlayerCard(
                    player: players[2],
                  ),
                  PlayerCard(
                    player: players[3],
                  ),
                  PlayerCard(
                    player: players[4],
                  ),
                ]),
            Spaces.verticalSpaceSmall,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                PlayerCard(
                  player: players[5],
                ),
                PlayerCard(
                  player: players[6],
                ),
                PlayerCard(
                  player: players[7],
                ),
                PlayerCard(
                  player: players[8],
                ),
              ],
            ),
            Spaces.verticalSpaceSmall,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                PlayerCard(
                  player: players[9],
                ),
                PlayerCard(
                  player: players[10],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
