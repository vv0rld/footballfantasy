enum PlayerPositions { GK, DEF, MID, ATT }

extension ParseToString on PlayerPositions {
  String toShortString() {
    return this.toString().split('.').last;
  }
}

// PlayerPositions playerPositionsFromInt(int index) {
//   return PlayerPositions.values.firstWhere(
//       (element) => element.index == PlayerPositions.values.indexOf(element));
// }
// values.indexWhere((element) => element.value[index]);

PlayerPositions playerPositionFromString(String value,
    {staring11, isSelected}) {
  return PlayerPositions.values
      .firstWhere((element) => element.toShortString() == value.toLowerCase());
}
